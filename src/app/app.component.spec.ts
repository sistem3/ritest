import { TestBed, async } from '@angular/core/testing';

import { AppComponent } from './app.component';
import { StoreComponent } from './store/store.component';
import { BasketComponent } from './basket/basket.component';

import { BasketService } from './basket/basket.service';
import { DiscountsService } from './shared/discounts.service';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        StoreComponent,
        BasketComponent
      ],
      providers: [BasketService, DiscountsService]
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it('should have a basket component', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('app-basket')).toBeTruthy();
  }));

  it('should have a store component', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('app-store')).toBeTruthy();
  }));
});
