import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { StoreComponent } from './store/store.component';
import { BasketComponent } from './basket/basket.component';

import { BasketService } from './basket/basket.service';
import { DiscountsService } from './shared/discounts.service';

@NgModule({
  declarations: [
    AppComponent,
    StoreComponent,
    BasketComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [BasketService, DiscountsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
