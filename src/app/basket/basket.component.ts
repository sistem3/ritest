import { Component, OnInit } from '@angular/core';
import { BasketService } from './basket.service';

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.css']
})
export class BasketComponent implements OnInit {

  basketProducts: any;
  basketTotal: number;
  basketShippingCost: number;

  constructor(private basketService: BasketService) { }

  ngOnInit() {
    this.basketService.basketStream$.subscribe((products) => {
      this.basketProducts = products;
    });

    this.basketService.basketTotalStream$.subscribe((total) => {
      this.basketTotal = total;
    });

    this.basketService.shippingCostStream$.subscribe((shippingCost) => {
      this.basketShippingCost = shippingCost;
    });
  }

}
