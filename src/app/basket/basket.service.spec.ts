import { TestBed, inject } from '@angular/core/testing';

import { BasketService } from './basket.service';
import { DiscountsService } from '../shared/discounts.service';

describe('BasketService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BasketService, DiscountsService]
    });
  });

  it('should be created', inject([BasketService], (service: BasketService) => {
    expect(service).toBeTruthy();
  }));

  it('should have a basket contents', inject([BasketService], (service: BasketService) => {
    expect(service.basketContents).toBeTruthy();
  }));

  it('should have a basket contents stream', inject([BasketService], (service: BasketService) => {
    expect(service.basketStream$).toBeTruthy();
  }));

  it('should have a basket total stream', inject([BasketService], (service: BasketService) => {
    expect(service.basketTotalStream$).toBeTruthy();
  }));

  it('should have a basket shipping cost stream', inject([BasketService], (service: BasketService) => {
    expect(service.shippingCostStream$).toBeTruthy();
  }));

  it('should be able to add products to the basket', inject([BasketService], (service: BasketService) => {
    const productStub = {id: 'B01', name: 'RI Mug', price: 4.99};
    service.addToBasket(productStub);
    expect(service.basketContents.length).toBe(1);
  }));

  it('should be able to calculate the total of a basket', inject([BasketService], (service: BasketService) => {
    service.basketContents = [{id: 'B01', name: 'RI Mug', price: 4.99}, {id: 'B01', name: 'RI Mug', price: 4.99}];
    const basketTotal = service.calculateTotal();
    expect(basketTotal).toBe(9.98);
  }));

  it('should be able to calculate the total shipping cost of a basket', inject([BasketService], (service: BasketService) => {
    service.basketContents = [{id: 'B01', name: 'RI Mug', price: 4.99}, {id: 'B01', name: 'RI Mug', price: 4.99}];
    const shippingTotal = service.calculateShippingCost();
    expect(shippingTotal).toBe(3.99);
  }));
});
