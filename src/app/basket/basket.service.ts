import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { DiscountsService } from '../shared/discounts.service';

@Injectable()
export class BasketService {

  private basketProducts = new Subject<any>();
  basketContents: any;
  basketStream$ = this.basketProducts.asObservable();

  private basketTotal = new Subject<number>();
  basketTotalStream$ = this.basketTotal.asObservable();

  private shippingCost = new Subject<number>();
  shippingCostStream$ = this.shippingCost.asObservable();

  constructor(private discountService: DiscountsService) {
    this.basketContents = [];
    this.basketProducts.next(this.basketContents);
    this.basketTotal.next(0);
    this.shippingCost.next(0);
  }

  addToBasket(product) {
    this.basketContents.push(product);
    this.basketProducts.next(this.basketContents);
    this.checkForDiscount();
    this.shippingCost.next(this.calculateShippingCost());
    this.basketTotal.next((this.calculateTotal() + this.calculateShippingCost()));
  }

  checkForDiscount() {
    let hasTrigger = false;
    let hasOfferItem = false;
    let discountAmount  = 0;
    let originalPrice = 0;
    let discountIndex;
    this.basketContents.forEach((item, index) => {
      this.discountService.discounts.forEach((offer) => {
        if (item.id === offer.discount.triggerId && !item.discountApplied) {
          hasTrigger = true;
        }

        if (item.id === offer.discount.productId && !item.discountApplied) {
          hasOfferItem = true;
          discountAmount = offer.discount.amount / 100;
          discountIndex = index;
          originalPrice = item.price;
        }
      });

      if (hasTrigger && hasOfferItem && !item.discountApplied) {
        this.basketContents[discountIndex].discountPrice = originalPrice * discountAmount;
        this.basketContents[discountIndex].discountApplied = true;
      }
    });
  }

  calculateTotal() {
    let total = 0;
    this.basketContents.forEach((item) => {
      if (item.discountPrice) {
        total = total + item.discountPrice;
      } else {
        total = total + item.price;
      }
    });
    return total;
  }

  calculateShippingCost() {
    let shippingPrice = 0;
    const totalPrice = this.calculateTotal();
    if (totalPrice < 10) {
      shippingPrice = 3.99;
    } else if (totalPrice > 10 && totalPrice < 30) {
      shippingPrice = 1.99;
    } else if (totalPrice > 30) {
      shippingPrice = 0;
    }
    return shippingPrice;
  }
}
