import { Injectable } from '@angular/core';

@Injectable()
export class DiscountsService {

  discounts: any;

  constructor() {
    this.discounts = [
      {
        id: 1,
        description: 'Buy \'RI the book\', get the video course half price',
        discount: {
          triggerId: 'M01',
          productId: 'V01',
          amount: 50
        }
      }
    ];
  }

}
