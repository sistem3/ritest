import { Injectable } from '@angular/core';

@Injectable()
export class ProductsService {

  products: any;

  constructor() {
    this.products = [
      {
        id: 'B01',
        name: 'RI Mug',
        price: 4.99
      },
      {
        id: 'M01',
        name: 'RI the book',
        price: 10.00
      },
      {
        id: 'V01',
        name: 'Video course on retail analytics',
        price: 29.99
      }
    ];
  }

}
