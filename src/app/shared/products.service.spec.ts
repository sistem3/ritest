import { TestBed, inject } from '@angular/core/testing';

import { ProductsService } from './products.service';

describe('ProductsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProductsService]
    });
  });

  it('should be created', inject([ProductsService], (service: ProductsService) => {
    expect(service).toBeTruthy();
  }));

  it('should have some products', inject([ProductsService], (service: ProductsService) => {
    expect(service.products).toBeTruthy();
  }));

  it('should have three products', inject([ProductsService], (service: ProductsService) => {
    expect(service.products.length).toBe(3);
  }));

  it('should have a product with the ID \'B01\'', inject([ProductsService], (service: ProductsService) => {
    expect(service.products[0].id).toBe('B01');
  }));

  it('should have a product with the title \'RI Mug\'', inject([ProductsService], (service: ProductsService) => {
    expect(service.products[0].name).toBe('RI Mug');
  }));

  it('should have a product with the price 4.99', inject([ProductsService], (service: ProductsService) => {
    expect(service.products[0].price).toBe(4.99);
  }));
});
