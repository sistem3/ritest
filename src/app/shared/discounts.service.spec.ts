import { TestBed, inject } from '@angular/core/testing';

import { DiscountsService } from './discounts.service';

describe('DiscountsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DiscountsService]
    });
  });

  it('should be created', inject([DiscountsService], (service: DiscountsService) => {
    expect(service).toBeTruthy();
  }));

  it('should have some discounts', inject([DiscountsService], (service: DiscountsService) => {
    expect(service.discounts).toBeTruthy();
  }));

  it('should have one discount', inject([DiscountsService], (service: DiscountsService) => {
    expect(service.discounts.length).toBe(1);
  }));

  it('should have discount with an ID', inject([DiscountsService], (service: DiscountsService) => {
    expect(service.discounts[0].id).toBeTruthy();
  }));

  it('should have discount with a description', inject([DiscountsService], (service: DiscountsService) => {
    expect(service.discounts[0].description).toBeTruthy();
  }));

  it('should have discount with a trigger item', inject([DiscountsService], (service: DiscountsService) => {
    expect(service.discounts[0].discount.triggerId).toBeTruthy();
  }));

  it('should have discount with a product item', inject([DiscountsService], (service: DiscountsService) => {
    expect(service.discounts[0].discount.productId).toBeTruthy();
  }));

  it('should have discount with a discount amount', inject([DiscountsService], (service: DiscountsService) => {
    expect(service.discounts[0].discount.amount).toBeTruthy();
  }));
});
