import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreComponent } from './store.component';
import { ProductsService } from '../shared/products.service';
import { BasketService } from '../basket/basket.service';
import { DiscountsService } from '../shared/discounts.service';

describe('StoreComponent', () => {
  let component: StoreComponent;
  let fixture: ComponentFixture<StoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StoreComponent ],
      providers: [ ProductsService, BasketService, DiscountsService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should have some products', () => {
    expect(component.storeProducts).toBeTruthy();
  });

  it('should have some discounts', () => {
    expect(component.storeDiscounts).toBeTruthy();
  });
});
