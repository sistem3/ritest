import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../shared/products.service';
import { BasketService } from '../basket/basket.service';
import { DiscountsService } from '../shared/discounts.service';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.css'],
  providers: [ ProductsService, DiscountsService ]
})
export class StoreComponent implements OnInit {

  storeProducts: any;
  storeDiscounts: any;

  constructor(
    private productService: ProductsService,
    private basketService: BasketService,
    private discountService: DiscountsService) { }

  ngOnInit() {
    this.storeProducts = this.productService.products;
    this.storeDiscounts = this.discountService.discounts;
  }

  addToBasket(product) {
    this.basketService.addToBasket(product);
  }
}
